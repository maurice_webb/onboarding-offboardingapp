import datetime

from django.db import models
from django.conf import settings
from django.db.models.deletion import CASCADE



class UserProfile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,null=True, blank=True)
    is_supervisor = models.BooleanField(default=False)
    
    
class Program(models.Model):
    name = models.CharField(max_length=200, default='example name')
    
    def __str__(self):
        return self.name

class Access(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    program = models.ForeignKey(Program, on_delete=models.CASCADE)
    
class AccessRequest(models.Model):
    employee = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='employee', on_delete=models.CASCADE)
    supervisor = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='supervisor', on_delete=models.CASCADE)
    
    def __str__(self):
        return "request:" + self.employee.username + "/" + self.supervisor.username
        
class ProgramRequest(models.Model):
    access_request = models.ForeignKey(AccessRequest, on_delete=models.CASCADE)
    program = models.ForeignKey(Program, on_delete=models.CASCADE)
    
