from django import forms
from .models import Program, ProgramRequest

from .models import AccessRequest

class AuthRequestForm(forms.ModelForm):
    programs = forms.ModelMultipleChoiceField(Program.objects.all())
    
    class Meta:
        model = AccessRequest
        fields = ('employee','supervisor','programs')

            