from django.shortcuts import render
from .forms import AuthRequestForm
from .models import ProgramRequest
#from .models import Access
#from .models import Programs

def index(request):
    context = {}
    form = AuthRequestForm(request.POST or None, request.FILES or None)
    context['form'] = form
    if request.method == 'POST':
        print(request.POST)
   
        if form.is_valid():
            new_access_request = form.save()
            # flip through the programs that were provided and create new ProgramRequest objects
            for program in form.cleaned_data['programs']:
                # create an empty ProgramRequest
                newRequest = ProgramRequest()
                # assign the access_request that was just created to it
                newRequest.access_request = new_access_request
                # assign the program to it
                newRequest.program = program
                # lock it into the database
                newRequest.save()
        
    return render(request, "index.html", context)



  
