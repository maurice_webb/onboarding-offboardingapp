from django.contrib import admin
from .models import ProgramRequest, UserProfile
from .models import Program
from .models import AccessRequest

admin.site.register(UserProfile)
admin.site.register(Program)
admin.site.register(AccessRequest)
admin.site.register(ProgramRequest)